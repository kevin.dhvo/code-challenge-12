package com.example.codechallenge12

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.example.codechallenge12.ui.theme.CodeChallenge12Theme
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CodeChallenge12Theme {
                KevinsContent()
            }
        }
    }
}

@Composable
fun KevinsContent() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                Brush.verticalGradient(
                    listOf(
                        MaterialTheme.colors.primary,
                        MaterialTheme.colors.secondary
                    )
                )
            ),
    ) {
        val messages: MutableList<String> = remember { mutableStateListOf<String>() }
        val arcRadius = with(LocalDensity.current) { 30.dp.toPx() }

        // Left side 'Padding'
        Column(modifier = Modifier
            .width(20.dp)
            .fillMaxHeight()
            .background(Color.White)
        ) {}

        // Right side 'padding'
        Column(modifier = Modifier
            .align(Alignment.TopEnd)
            .width(20.dp)
            .fillMaxHeight()
            .background(Color.White)
        ) {}

        Column(modifier = Modifier.fillMaxSize()) {
            LazyColumn(
                modifier = Modifier.fillMaxWidth(),
                contentPadding = PaddingValues(horizontal = 20.dp)
            ) {
                item {
                    // Top 'padding'
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(20.dp)
                            .background(Color.White)
                    ) {}
                }
                items(messages) { message ->
                    Box(
                        modifier = Modifier
                            .padding(bottom = 20.dp)
                            .drawBehind {
                                clipPath(
                                    path = drawMessagePath(size, arcRadius),
                                    clipOp = ClipOp.Difference
                                ) {
                                    drawRect(
                                        Color.White,
                                        size = Size(size.width * 2, size.height + 20.dp.toPx())
                                    )
                                }
                            }
                    ) {
                        Message(message.toString())
                    }
                }
            }

            // Bottom 'padding'
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {}
        }

        // Fun random stuff
        fun getRandomString(length: Int) : String {
            val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }
        fun getRandomInt(limit: Int): Int {
            return (1..limit).random()
        }

        // FAB add
        FloatingActionButton(
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(20.dp),
            onClick = { messages.add(getRandomString(getRandomInt(200))) }
        ) {
            Icon(Icons.Default.Add, null)
        }
    }
}

@Composable
private fun Message(msg: String) {
    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth(0.5f)
            .heightIn(min = 40.dp)
            .background(
                Color.Transparent, MessageShape(
                    with(LocalDensity.current) { 30.dp.toPx() })
            ),
        contentAlignment = Alignment.CenterStart
    ) {
        Text(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 25.dp, end = 10.dp)
                .padding(vertical = 10.dp),
            text = msg
        )
    }
}

class MessageShape(private val arcRadius: Float) : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        return Outline.Generic(
            path = drawMessagePath(size = size, arcRadius = arcRadius)
        )
    }
}

private fun drawMessagePath(size: Size, arcRadius: Float) : Path {
    return Path().apply {
        reset()
        // starting from bottom left
        moveTo(0f, size.height)
        lineTo(size.width - arcRadius, size.height)
        arcTo(
            rect = Rect(
                left = size.width - arcRadius,
                right = size.width,
                top = size.height - arcRadius,
                bottom = size.height
            ),
            startAngleDegrees = 90f,
            sweepAngleDegrees = -90f,
            forceMoveTo = false
        )
        lineTo(size.width, 0f + arcRadius)
        arcTo(
            rect = Rect(
                left = size.width - arcRadius,
                right = size.width,
                top = 0f,
                bottom = 0f + arcRadius
            ),
            startAngleDegrees = 0f,
            sweepAngleDegrees = -90f,
            forceMoveTo = false
        )
        lineTo(0f + arcRadius/2,0f)
        arcTo(
            rect = Rect(
                left = 0f+arcRadius/2,
                right = arcRadius+arcRadius/2,
                top = 0f,
                bottom = 0f + arcRadius
            ),
            startAngleDegrees = -90f,
            sweepAngleDegrees = -90f,
            forceMoveTo = false
        )
        lineTo(0f+arcRadius/2, size.height - arcRadius)
        arcTo(
            rect = Rect(
                left = -arcRadius+arcRadius/2,
                right = 0f+arcRadius/2,
                top = size.height - arcRadius,
                bottom = size.height
            ),
            startAngleDegrees = 0f,
            sweepAngleDegrees = 90f,
            forceMoveTo = false
        )
        close()
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CodeChallenge12Theme {
        KevinsContent()
    }
}
